package com.yasuda.app;

import com.yasuda.app.thymeleaf.web.MailController;
import java.util.List;
import java.util.Map;
import java.time.LocalDateTime;

import org.springframework.scheduling.annotation.Async;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.stereotype.Controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.SimpleMailMessage;

@Controller
// @RestController
public class DbConnect {

    private final JavaMailSender javaMailSender;
    LocalDateTime ldt;

    String taskTitle;
    String description;
    String status;
    Integer member_id;
    Integer task_id;

    @Autowired
    DbConnect(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Autowired
	JdbcTemplate jdbcTemplate;

    @PostMapping(path="/insert_task", produces="application/json")
    public String insert_task(@RequestParam Map<String,String> requestParams) {

        String taskTitle = requestParams.get("taskTitle");
        String description = requestParams.get("description");
        String status = requestParams.get("status");
        Integer member_id = Integer.parseInt(requestParams.get("member_id"));
        
        ldt = LocalDateTime.now().plusHours(9);
        jdbcTemplate.update("insert into task (title, message, updated_time, updated_user_id, status, member_id) values (?, ?, ?, 1, ?::status_enum, ?)"
        ,taskTitle, description, ldt, status, member_id);

        String sendAddress = jdbcTemplate.queryForObject("SELECT email FROM task_users WHERE id = ?", String.class, member_id);
        
        new MailController(javaMailSender).sendMailCon(taskTitle, description, sendAddress);

   
        return "redirect:/";

    }

    @PostMapping(path="/update_task", produces="application/json")
    public String update_task(@RequestParam Map<String,String> requestParams) {

        taskTitle = requestParams.get("taskTitle");
        description = requestParams.get("description");
        status = requestParams.get("status");
        member_id = Integer.parseInt(requestParams.get("member_id"));
        task_id = Integer.parseInt(requestParams.get("task_id"));
        ldt = LocalDateTime.now().plusHours(9);
        
        jdbcTemplate.update("update task set title = ?, message = ?, updated_time = ?, updated_user_id = 1, status = ?::status_enum, member_id = ? where id = ?"
        ,taskTitle, description, ldt, status, member_id, task_id);
            
        String sendAddress = jdbcTemplate.queryForObject("SELECT email FROM task_users WHERE id = ?", String.class, member_id);
        new MailController(javaMailSender).sendMailCon(taskTitle, description, sendAddress);
   
        return "redirect:/";
    }


    @RequestMapping("/select_task")
    @ResponseBody
    public List<Map<String,Object>> select_task(){

        String sql = "SELECT t.id, t.title, t.message, t.updated_time, t.updated_user_id, t.status, t.member_id, tu1.name as updated_user_name, tu2.name as assin_user";
        sql += " FROM task t INNER JOIN task_users tu1 ON tu1.id = t.updated_user_id";
        sql += " INNER JOIN task_users tu2 ON tu2.id=t.member_id order by t.id";

        return jdbcTemplate.queryForList(sql);

    }

    @RequestMapping("/select_user")
    @ResponseBody
    public List<Map<String,Object>> select_user(){

        String sql = "SELECT id, name FROM task_users order by id";

        return jdbcTemplate.queryForList(sql);

    }

    @RequestMapping("/init_user")
    @ResponseBody
    public List<Map<String,Object>> init_user(@AuthenticationPrincipal OidcUser user){

        jdbcTemplate.update("INSERT INTO task_users (name, email) VALUES (?, ?) ON CONFLICT DO NOTHING",user.getFullName(),user.getEmail());

        return jdbcTemplate.queryForList("SELECT id, name FROM task_users order by id");
    }

}
