package com.yasuda.app.thymeleaf.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.SimpleMailMessage;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


// @Controller
@RestController
public class MailController {

    private JavaMailSender javaMailSender;

    @Autowired
    public MailController(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMailCon(String taskTitle, String description, String sendAddress) {

        SimpleMailMessage msg = new SimpleMailMessage();

        msg.setFrom("taskmailsend@gmail.com");
        msg.setTo(sendAddress);
		// msg.setBcc("送信先アドレス2");
		// msg.setCc(new String[] {"送信先アドレス3", "送信先アドレス4"});
		msg.setSubject(taskTitle);
		msg.setText(description);
        javaMailSender.send(msg);

    }

    // @Value("${send_email_cron}")
    // static final String sendEmailCron;

    // @GetMapping("/mail")
    // @RequestMapping(path = "/mail",method = {RequestMethod.POST})
    // public String sendMailCon() {
    //     SimpleMailMessage msg = new SimpleMailMessage();
    //     msg.setFrom("taskmailsend@gmail.com");
	// 	msg.setTo("yaya.20190106@gmail.com");
	// 	// msg.setBcc("送信先アドレス2");
	// 	// msg.setCc(new String[] {"送信先アドレス3", "送信先アドレス4"});
	// 	msg.setSubject("メールタイトル");
	// 	msg.setText("送信テスト");
    //     javaMailSender.send(msg);

    //     return "送信しました。";
    // }

    // public String autosend() {
    //     SimpleMailMessage msg = new SimpleMailMessage();
    //     msg.setFrom("taskmailsend@gmail.com");
	// 	msg.setTo("yaya.20190106@gmail.com");
	// 	// msg.setBcc("送信先アドレス2");
	// 	// msg.setCc(new String[] {"送信先アドレス3", "送信先アドレス4"});
	// 	msg.setSubject("メールタイトル");
	// 	msg.setText("送信テスト");
    //     javaMailSender.send(msg);

    //     return "メール送信しました";
	// }

    // @Scheduled(cron = "${send_email_cron}", zone = "Asia/Tokyo")
    // @Scheduled(cron = "0 22 23 * * *", zone = "Asia/Tokyo")
    // @Scheduled(fixedDelay = 300000) //5分毎
    // public void autoprocess() {
        // System.out.println("${send_email_cron}"+"aaa");

    //         autosend();
    // }

}