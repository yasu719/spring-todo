package com.yasuda.app;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Spring MVCコントローラとしてマークする、@RestControllerとの違いはデフォルトで@ResponseBodyが含まれないこと。
// @ResponseBodyを付けると戻り値で直接レスポンスのコンテンツを返す事ができる。（文字列をそのまま表示させるときなど）
@Controller
// @SpringBootApplicationは、このアノテーションは右記のアノテーションを一つにまとめたもの。@EnableAutoConfiguration、@ComponentScan、@Configuration
@SpringBootApplication
public class AppApplication {

    @RequestMapping("/")
    public String home(Model model, @AuthenticationPrincipal OidcUser user) {
    model.addAttribute("username", user.getFullName());

    return "task";
    }

	public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);   
        
	}

}
